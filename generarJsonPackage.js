const fse = require('fs-extra');


let firstPart;


function initialize() {
    firstPart = ``;
}

environments = ` DATA_BASE_HOST= \n USER= \n PASSWORD=\n DATA_BASE=\n`;


function generaPackage(json) {

    firstPart = ``;
    firstPart = `{
            "name": "microservices${json.table_name}",
            "version": "1.0.0",
            "description": "",
            "main": "index.js",
            "scripts": {
                "start": "node index"
            },
            "author": "",
            "license": "ISC",
            "dependencies": {
                "body-parser": "^1.19.0",
                "cors": "^2.8.5",
                "dotenv": "^8.2.0",
                "express": "^4.17.1",
                "mariadb": "^2.3.1"
            }
        }`;


}


const generateFile = async(data, path) => {
    initialize();
    for (let i = 0; i < data.length; i++) {
        await generaPackage(data[i]);
        await fse.outputFile(`${path}/${data[i].table_name.toUpperCase()}/package.json`, firstPart)
            .catch(err => {
                return err;
            });
        await fse.outputFile(`${path}/${data[i].table_name.toUpperCase()}/.env`, environments)
            .catch(err => {
                return err;
            });
    }

}

exports.generateFile = generateFile;