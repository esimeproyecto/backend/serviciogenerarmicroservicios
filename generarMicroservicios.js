const fse = require('fs-extra');


let firstPart;
let lastPart;


function initialize() {
    firstPart = `const express = require('express');
    const app = express();
    const dotenv = require('dotenv');
    dotenv.config();
    const bodyParser = require('body-parser');
    var cors = require('cors');
    var mariadb = require('mariadb');
    const port = process.env.PORT || 3000;
    
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    app.use(cors());
    
    const pool = mariadb.createPool({
        host: process.env.DATA_BASE_HOST,
        user: process.env.USER,
        password: process.env.PASSWORD,
        database: process.env.DATA_BASE,
        connectionLimit: 5
    }); \n`;

    lastPart = `
    app.listen(port, function() {
        console.log('El sitio de APIs inició correctamente en el puerto: ', port);
    });`;
}



function generaPackage(json) {

    firstPart += `app.post('/getEndPoint${json.table_name.toUpperCase()}', async function(req, res) {

        let conn;
        let row;
    
        try {
            conn = await pool.getConnection();
            row = await conn.query ("SELECT `;
    json.columns.forEach((columna, i) => {
        // console.log(json.columns.length);
        if (i + 1 == json.columns.length) {
            firstPart += `${columna.Field || columna.field}`
        } else {
            firstPart += `${columna.Field || columna.field},`
        }
    });
    firstPart += ` FROM ${json.table_name};");
            res.json(row);\n
            conn.release();
        } catch (err) {
            conn.release();
            throw err;
        };

    });\n\n`;


    firstPart += `app.post('/insertEndPoint${json.table_name.toUpperCase()}', async function(req, res) {

        let conn;
        let row;
        let filtro = req.body;
    
        try {
            conn = await pool.getConnection();
            row = await conn.query ( ${ "`INSERT INTO " + json.table_name} (`;
    json.columns.forEach((columna, i) => {
        // console.log(json.columns.length);
    
        if (i + 1 == json.columns.length) {
            if (columna.Extra === 'auto_increment' || columna.extra === 'auto_increment') {

            } else {
            firstPart += `${columna.Field || columna.field}`
                }
        } else {
            if (columna.Extra === 'auto_increment' || columna.extra === 'auto_increment') {

            } else {
            firstPart += `${columna.Field || columna.field},`
                }
        }
    });

    firstPart += ` ) VALUES \n ( `;

    json.columns.forEach((columna, i) => {
        // console.log(json.columns.length);
        if (i + 1 == json.columns.length) {
            if (columna.Extra === 'auto_increment' || columna.extra === 'auto_increment') {

            } else {
            firstPart += `${"'${filtro." + columna.Field || columna.field} }'`
                }

        } else {
            if (columna.Extra === 'auto_increment' || columna.extra === 'auto_increment') {

            } else {
            firstPart += `'${"${filtro." + columna.Field || columna.field} }',`
                }
        }
    });
    firstPart += ` ); ${"`"});
    res.json(row);\n
    conn.release();
    } catch (err) {
    conn.release();
    throw err;
    };

    });\n\n`;


    firstPart += `app.post('/updateEndPoint${json.table_name.toUpperCase()}', async function(req, res) {

        let conn;
        let row;
        let filtro = req.body;
    
        try {
            conn = await pool.getConnection();
            row = await conn.query ( ${ "`UPDATE  " + json.table_name}  SET  `;

     json.columns.forEach((columna, i) => {
        // console.log(json.columns.length);
    
        if (i + 1 == json.columns.length) {
            if (columna.Extra === 'auto_increment' || columna.extra === 'auto_increment') {

            } else {
                firstPart += `${columna.Field || columna.field} ${" =  '${filtro." + columna.Field || columna.field }}' \n `
                }
        } else {
            if (columna.Extra === 'auto_increment' || columna.extra === 'auto_increment') {

            } else {
                firstPart += `${columna.Field || columna.field} ${" =  '${filtro." + columna.Field || columna.field }}', \n `
                }
        }
    });

    firstPart += ` WHERE ${json.columns[0].Field || json.columns[0].Field} ${"='${filtro." + json.columns[0].Field} }'; ${"`);"} \n
    res.json(row);
    conn.release();
    } catch (err) {
    conn.release();
    throw err;
    };
    
    });\n\n`;


    firstPart += `\napp.post('/deleteEndPoint${json.table_name.toUpperCase()}', async function(req, res) {

        let conn;
        let row;
        let filtro = req.body;
    
        try {
            conn = await pool.getConnection();
            row = await conn.query ( ${ "`DELETE FROM  " + json.table_name}
            WHERE  ${json.columns[0].Field || json.columns[0].field} = ${ "'${filtro." + json.columns[0].Field || json.columns[0].field} }'; ${"`);"} 
            res.json(row);
            conn.release();
    } catch (err) {
        conn.release();
        throw err;
    };

}); \n`;

}


const generateFile = async(data, path) => {
    for (let i = 0; i < data.length; i++) {
        initialize();
        // data[i].table_name = data[i].table_name.replace(/\s+/g, '');
        // data[i].table_name = data[i].table_name.replace(/[^a-zA-Z0-9]/g, '');
        await generaPackage(data[i]);
        template = firstPart + lastPart;
        await fse.outputFile(`${path}/${data[i].table_name.toUpperCase()}/index.js`, template)
            .catch(err => {
                return err;
            });
    }

}

exports.generateFile = generateFile;