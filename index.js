const express = require('express');
const app = express();
const bodyParser = require('body-parser');
var uuid = require('uuid-random');
var cors = require('cors');
var generarJson = require('./generarJsonPackage');
var generarMicroservices = require('./generarMicroservicios');
var zip = require('bestzip');
const fs = require('fs');
const path = require('path');

const port = process.env.PORT || 4900;


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

app.post('/generar', async function(req, res) {
    console.log("entro");
    let data = req.body;
    let folderName = uuid(); //declara nombre unico
    try {
        //crea folder con nombre unico
        await fs.mkdir(`./MicroserviciosGenerados/${folderName}`, { recursive: true }, err => {});

        var absolutPath = path.resolve(`./MicroserviciosGenerados/${folderName}`);

        await generarMicroservices.generateFile(data, absolutPath);

        await generarJson.generateFile(data, absolutPath);


    } catch (err) {
        res.json({ status: false });
        throw console.log(err);
    }
    res.json({ status: true, file: absolutPath });
});

app.listen(port, function() {
    console.log('El sitio de APIs inició correctamente en el puerto: ', port);
});